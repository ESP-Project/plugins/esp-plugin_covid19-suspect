'''
                                  ESP Health
                         Notifiable Diseases Framework
                            COVID-19 SUSPECT Case Generator


@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: Commonwealth Informatics.
@contact: http://esphealth.org
@copyright: (c) 2020 Commonwealth Informatics
@license: LGPL
'''
import datetime
from django.db.models import Count, Q
from ESP.utils import log
from ESP.utils.utils import queryset_iterator
from ESP.hef.base import Event, BaseEventHeuristic, LabResultAnyHeuristic, LabOrderHeuristic
from ESP.hef.base import LabResultPositiveHeuristic, Dx_CodeQuery, LabResultNoEventHeuristic, HEF_CORE_URI
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case
from ESP.conf.models import ConditionConfig, LabTestMap
from ESP.static.models import DrugSynonym
from ESP.emr.models import Encounter, LabOrder
from django.contrib.contenttypes.models import ContentType
from dateutil.relativedelta import relativedelta

FEVER=100

class Covid19LabOrderHeuristic(LabOrderHeuristic):
    '''
    extension of LabOrderHeuristic to use procedure_name OR procedure_code 
    '''
    def __init__(self,test_name):
        super(Covid19LabOrderHeuristic,self).__init__(test_name)

    def __hash__(self):
        return hash(self.test_name)
    
    @property
    def _lab_test_maps(self):
        testmaps = LabTestMap.objects.filter(test_name=self.test_name)
        if not testmaps:
            log.warning('No tests mapped for "%s". Cannot generate events' % self.test_name)
        testmaps = testmaps.filter( Q(record_type='result') | Q(record_type='both'))
        return testmaps

    def generate(self):
        log.info('Generating events for %s' % self)
        f1 = Q(procedure_code__in=self._lab_test_maps.values('native_code'))
        f2 = Q(procedure_name__in=self._lab_test_maps.values('native_code'))
        alt = LabOrder.objects.filter(f1 | f2)
        unbound_orders = alt.exclude(events__name=self.order_event_name)
        counter = 0
        for order in queryset_iterator(unbound_orders):
            Event.create(
                name = self.order_event_name,
                source = self.uri,
                date = order.date,
                patient = order.patient,
                provider = order.provider,
                emr_record = order,
                )
            counter += 1
        log.info('Generated %s new %s events' % (counter, self))
        return counter


class FeverDiagnosisHeuristic(BaseEventHeuristic):
    '''
    A heuristic for detecting events based on fever diagnosis codes
    from a physician encounter, ensuring that the encounter temperature value is null
    '''
    def __init__(self, name, dx_code_queries):
        '''
        @param name: Name of this heuristic
        @type name:  String
        @param dx_code_queries: Generate event for records matching one of these queries
        @type dx_code_queries:  List of dx codes Query objects
        '''
        assert name and dx_code_queries
        self.name = name
        self.dx_code_queries = dx_code_queries

    def __hash__(self):
        return hash(self.name)

    @property
    def short_name(self):
        sn = u'diagnosis:%s' % self.name
        return sn
    
    uri = u'urn:x-esphealth:heuristic:channing:diagnosis:v1'
    
    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]
    
    def __str__(self):
        return self.short_name
    
    @property
    def encounters(self):
        
        enc_q = self.dx_code_queries[0].encounter_q_obj
        for query in self.dx_code_queries[1:]:
            enc_q |= query.encounter_q_obj
        return Encounter.objects.filter(enc_q)
    
    @property
    def dx_event_name(self):
        return 'dx:%s' % self.name
    
    @property
    def event_names(self):
        return [self.dx_event_name]
    
    def generate(self):
        #get the set of encounter PKs with fever diagnosis
        enc_qs_set = set(self.encounters.values_list('pk',flat=True))
        #get the set of encounter PKs attached to dx:fever events
        excl_qs_set = set(Event.objects.filter(name__in=self.event_names).values_list('object_id',flat=True))
        #get the list of encounter pk and temp values for the fever diagnosis encounters that are not events (yet)
        enc_pks_temp_tups = Encounter.objects.filter(pk__in=enc_qs_set - excl_qs_set).values_list('pk','temperature')
        #go through the list of tuples and if there is no temperature, then keep the pk.
        evnt_pks = set()
        for pk_tup in enc_pks_temp_tups:
             if not pk_tup[1]:
                 evnt_pks.add(pk_tup[0])
        #now get the set of encounter events for those pks, which should be new events.
        enc_qs = Encounter.objects.filter(pk__in=evnt_pks).order_by('date')
        log.info('Generating events for "%s"' % self)
        #log_query('Encounters for %s' % self, enc_qs)
        counter = 0
        for enc in queryset_iterator(enc_qs):
            Event.create(
                name = self.dx_event_name,
                source = self.uri,
                patient = enc.patient,
                date = enc.date,
                provider = enc.provider,
                emr_record = enc,
                )
            counter += 1
        log.info('Generated %s new events for %s' % (counter, self))
        return counter

class EncounterFeverHeuristic(BaseEventHeuristic):
    '''
    Fever Event Heuristic
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name

    def __hash__(self):
        return hash(self.name)

    @property
    def short_name(self):
        return u'enc:%s' % self.name

    uri = u'urn:x-esphealth:heuristic:channing:encbp:v1'
    
    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]

    @property
    def encounters(self):
        fev_enc_pk_lst = Encounter.objects.filter(temperature__gte = FEVER).values_list('pk',flat=True)
        fev_evnt_objid_lst = Event.objects.filter(name='enc:fever').values_list('object_id',flat=True)
        pk_lst = list(set(fev_enc_pk_lst)-set(fev_evnt_objid_lst))
        return Encounter.objects.filter(pk__in=pk_lst)

    @property
    def enc_event_name(self):
        return 'enc:%s' % self.name 

    @property
    def event_names(self):
        return [self.enc_event_name]

    def generate(self):
        log.info('Generating events for "%s"' % self)
        counter = 0
        for enc in queryset_iterator(self.encounters):
            Event.create(name = self.enc_event_name,
                    source=self.uri,
                    patient = enc.patient,
                    date = enc.date,
                    provider = enc.provider,
                    emr_record = enc,
                    )
            counter += 1

        log.info('Generated %s new events for %s' % (counter, self))
        return counter


class DiagnosisHeuristic(BaseEventHeuristic):
    '''
    A heuristic for detecting events based on one or more diagnosis codes
    from a physician encounter.
    '''
    def __init__(self, name, dx_code_queries):
        '''
        @param name: Name of this heuristic
        @type name:  String
        @param dx_code_queries: Generate event for records matching one of these queries
        @type dx_code_queries:  List of dx codes Query objects
        '''
        assert name and dx_code_queries
        self.name = name
        self.dx_code_queries = dx_code_queries

    def __hash__(self):
        return hash(self.name)

    @property
    def short_name(self):
        sn = u'diagnosis:%s' % self.name
        return sn
    
    uri = u'urn:x-esphealth:heuristic:channing:diagnosis:v1'
    
    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]
    
    def __str__(self):
        return self.short_name
    
    @property
    def encounters(self):
        #local mod to just return set of enc pks
        enc_q = self.dx_code_queries[0].encounter_q_obj
        for query in self.dx_code_queries[1:]:
            enc_q |= query.encounter_q_obj
        return set(Encounter.objects.filter(enc_q).values_list('pk',flat=True))
    
    @property
    def dx_event_name(self):
        return 'dx:%s' % self.name
    
    @property
    def event_names(self):
        return [self.dx_event_name]
    
    def generate(self):
        enc_pkset = self.encounters
        pk_lst = list(enc_pkset - set(Event.objects.filter(name__in=self.event_names).values_list('object_id',flat=True)))
        enc_qs = Encounter.objects.filter(pk__in=pk_lst).values('pk','patient','date','provider')
        log.info('Generating events for "%s"' % self)
        #log_query('Encounters for %s' % self, enc_qs)
        counter = 0
        content_type = ContentType.objects.get_for_model(Encounter)
        for enc in enc_qs:
            e = Event(
                name = self.dx_event_name,
                source = self.uri,
                date = enc['date'],
                patient_id = enc['patient'],
                provider_id = enc['provider'],
                content_type = content_type,
                object_id = enc['pk'],
                )
            e.save()
            counter += 1
        log.info('Generated %s new events for %s' % (counter, self))
        return counter

class covid19_suspect(DiseaseDefinition):
    '''
    covid-19 case definition
    '''
    
    conditions = ['covid19_suspect']

    uri = 'urn:x-esphealth:disease:commoninf:covid19_suspect:v1'
    
    short_name = 'covid19_suspect'
    
    test_name_search_strings = [ 'covid', 'sars', 'cov', 'corona', 'flu', 'influenza', 'wbc', 'white bl', 'platelet', 'plt', 'hematocrit', 'hct', 'lymph', 'pneumo', 'adenovirus', 'rhinovirus', 'enterovirus', 'rsv', 'syncytial', 'syncitial', 'synctial', 'parapertussis', 'reactive', 'dimer', 'po2', 'partial', 'parainflu', 'crp']
    
    timespan_heuristics = []
    
    recurrence_interval = 42
    
    status='AR'
    
    @property
    def event_heuristics(self):
        heuristic_list = []
       
        #
        # Diagnosis Codes for covid-19
        #
        # FROM COVID19
        heuristic_list.append( DiagnosisHeuristic(
            name = 'covid19',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='B34.2', type='icd10'),
                 Dx_CodeQuery(starts_with='B97.29', type='icd10'),
                 Dx_CodeQuery(starts_with='U07.1', type='icd10'),
                ]))

        #
        # Diagnosis Codes for Contact with and exposure to other communicable disease
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'contact-communicable-disease',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='Z20.828', type='icd10'),
                ]))

        #
        # Generate events for measured fever
        #
        heuristic_list.append( EncounterFeverHeuristic(
            name = 'fever'
            ))

        #
        # Diagnosis Codes for fever
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'covid_suspect:fever',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='R50.81', type='icd10'),
                 Dx_CodeQuery(starts_with='R50.9', type='icd10'),
                 Dx_CodeQuery(starts_with='R56.00',type='icd10'),
                ]))

        #
        # Diagnosis Codes for chills 
        #
        # FROM ANAPLASMOSIS
        heuristic_list.append( DiagnosisHeuristic(
            name = 'chills',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='780.64', type='icd9'),
                Dx_CodeQuery(starts_with='R68.83', type='icd10'),
                ]))

        #
        # Diagnosis Codes for myalgia 
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'covid_suspect:myalgia',
            dx_code_queries = [
                Dx_CodeQuery(ends_with='M79.1', type='icd10'),
                Dx_CodeQuery(starts_with='M79.10', type='icd10'),
                Dx_CodeQuery(starts_with='M79.18', type='icd10'),
                ]))

        #
        # Diagnosis Codes for headache 
        #
        # FROM ANAPLASMOSIS
        heuristic_list.append( DiagnosisHeuristic(
            name = 'headache',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='784.0', type='icd9'),
                Dx_CodeQuery(starts_with='R51', type='icd10'),
                ]))


        #
        # Diagnosis Codes for sore throat 
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'sore-throat',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='R07.0', type='icd10'),
                ]))

        #
        # Diagnosis Codes for olfactory and taste disorder(s)
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'olfactory-taste-disorders',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='R43.0', type='icd10'),
                 Dx_CodeQuery(starts_with='R43.1', type='icd10'),
                 Dx_CodeQuery(starts_with='R43.2', type='icd10'),
                 Dx_CodeQuery(starts_with='R43.8', type='icd10'),
                 Dx_CodeQuery(starts_with='R43.9', type='icd10'),
                 ]))

        #
        # Diagnosis Codes for cough
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'covid_suspect:cough',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='R05', type='icd10'),
                 ]))

        #
        # Diagnosis Codes for shortness of breath
        #
        # FROM COVID19
        heuristic_list.append( DiagnosisHeuristic(
            name = 'shortofbreath',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='R06.02', type='icd10'),
                 ]))
                 
        #
        # Diagnosis Codes for difficulty breathing
        #
        # 
        heuristic_list.append( DiagnosisHeuristic(
            name = 'difficulty_breathing',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='R06.89', type='icd10'),
                 Dx_CodeQuery(starts_with='R06.9', type='icd10'),
                 Dx_CodeQuery(starts_with='R07.1', type='icd10'),
                 ]))

        #
        # Diagnosis Codes for Pneumonia
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'covid_suspect:pneumonia',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='J12.8', type='icd10'),
                 Dx_CodeQuery(starts_with='J12.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J18.8', type='icd10'),
                 Dx_CodeQuery(starts_with='J18.9', type='icd10'),
                ]))

        #
        # Diagnosis Codes for acute respiratory distress syndrome (ards)
        #
        # FROM COVID19
        heuristic_list.append( DiagnosisHeuristic(
            name = 'ards',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='J80', type='icd10'),
                 ]))
                 
        #
        # LabOrders for Chest CT Scan
        #
        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'chestct',
             ))

        #
        # LabOrders for Chest xray Scan
        #
        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'chestxray',
             ))
             
                #
        # Procedures for Mechanical Ventilation 
        # Found Within emr_laborder
        #
        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'mechanical_vent',
             ))   

        # Convalescent Plasma 
        # Found Within emr_laborder
        #
        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'conva_plasma',
             ))                  
            
        #
        # LabResults for COVID-19
        #
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'covid19_pcr',
             ))

        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'covid19_igg',
             ))

        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'covid19_igm',
             ))

        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'covid19_iga',
             ))

        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'covid19_ab_total',
             ))
             
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'covid19_ag',
             ))

        #
        # LabOrders for COVID-19
        #
        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'covid19_pcr_order',
             ))

        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'covid19_igg_order',
             ))

        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'covid19_igm_order',
             ))

        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'covid19_iga_order',
             ))

        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'covid19_ab_total_order',
             ))
             
        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'covid19_ag_order',
             ))

        #
        # LabResults for Influenza 
        #

        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'influenza',
             ))

        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'influenza_culture',
             ))

        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'rapid_flu',
             ))
             
        #
        # Lymphocytes
        #        
        heuristic_list.append(LabResultNoEventHeuristic(
            test_name= 'lymphocytes',
            ))
            
        #
        # M. pneumoniae &  C. pneumoniae
        #        
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name= 'm_pneumoniae_igm',
            ))
         
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name= 'm_pneumoniae_pcr',
            ))
        
        #Titer Result 1:10        
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='c_pneumoniae_igm',
            titer_dilution=10,
            ))

            
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name= 'c_pneumoniae_pcr',
            ))
            
        #
        # Respiratory syncytial virus
        #        
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name= 'rsv',
            ))
            
        #
        # Bordatella parapertussis
        #        
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name= 'parapertussis',
            ))
            
        #
        # Coronavirus (OC43, 229E, HKU1, NL63)
        # EVERYTHING BUT COVID-19       
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name= 'coronavirus_non19',
            ))
            
        #
        # Rhinovirus/Enterovirus
        #        
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name= 'rhino_entero_virus',
            ))
            
        #
        # Adenovirus
        #        
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name= 'adenovirus',
            ))
            
        #
        # Parainfluenza
        #        
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name= 'parainfluenza',
            ))
            
        #
        # H. metapneumovirus 
        #        
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name= 'h_metapneumovirus',
            ))

        #
        # C-reactive protein - Map tests but do not generate events
        #        
        heuristic_list.append(LabResultNoEventHeuristic(
            test_name= 'crp',
            ))
            
        #
        # d-dimer - Map tests but do not generate events
        #        
        heuristic_list.append(LabResultNoEventHeuristic(
            test_name= 'd_dimer',
            ))
            
        #
        #  pO2 - Map tests but do not generate events
        #        
        heuristic_list.append(LabResultNoEventHeuristic(
            test_name= 'po2_art',
            ))
            
        heuristic_list.append(LabResultNoEventHeuristic(
            test_name= 'po2_ven',
            ))

        return heuristic_list

    def generate(self):
        log.info('Generating cases of %s' % self.short_name) 


    
            
#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------

covid19_suspect_definition = covid19_suspect()

def event_heuristics():
    return covid19_suspect_definition.event_heuristics

def disease_definitions():
    return [covid19_suspect_definition]
