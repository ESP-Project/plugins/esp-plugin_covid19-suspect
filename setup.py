'''
                                  ESP Health
                          COVID-19 Suspect Disease Definition
                             Packaging Information
                                  
@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: Commonwealth Informatics.
@contact: http://esphealth.org
@copyright: (c) 2020 Commonwealth Informatics
@license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
'''

from setuptools import setup
from setuptools import find_packages

setup(
    name = 'esp-plugin-covid19-suspect',
    version = '1.4',
    author = 'Karen Eberhardt',
    author_email = 'keberhardt@commoninf.com',
    description = 'COVID-19 suspect definition module for ESP Health application',
    license = 'LGPLv3',
    keywords = 'COVID-19-suspect algorithm disease surveillance public health epidemiology',
    url = 'http://esphealth.org',
    packages = find_packages(exclude=['ez_setup']),
    install_requires = [
        ],
    entry_points = '''
        [esphealth]
        disease_definitions = covid19_suspect:disease_definitions
        event_heuristics = covid19_suspect:event_heuristics
    '''
    )
